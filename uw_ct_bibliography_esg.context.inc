<?php

/**
 * @file
 * uw_ct_bibliography_esg.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_bibliography_esg_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'publications_related_to_projects';
  $context->description = 'Biblio publications related to projects';
  $context->tag = 'Content';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'uw_project' => 'uw_project',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-738cc51d73bf0c3f4f4451fb56519a0a' => array(
          'module' => 'views',
          'delta' => '738cc51d73bf0c3f4f4451fb56519a0a',
          'region' => 'content',
          'weight' => '20',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Biblio publications related to projects');
  t('Content');
  $export['publications_related_to_projects'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'recent_publications';
  $context->description = 'A block with recent publications';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '*' => '*',
        '~publications' => '~publications',
        '~publications/*' => '~publications/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-uw_publications-block_1' => array(
          'module' => 'views',
          'delta' => 'uw_publications-block_1',
          'region' => 'sidebar_second',
          'weight' => '20',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('A block with recent publications');
  t('Content');
  $export['recent_publications'] = $context;

  return $export;
}
