<?php

/**
 * @file
 * uw_ct_bibliography_esg.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_bibliography_esg_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: context.
  $overrides["context.publications_related_to_people.reactions|block|blocks|views-7b25f6c74092e59ef43f1fdcd8aaedb9"] = array(
    'module' => 'views',
    'delta' => '7b25f6c74092e59ef43f1fdcd8aaedb9',
    'region' => 'content',
    'weight' => 21,
  );
  $overrides["context.publications_related_to_people.reactions|block|blocks|views-uw_publications-block_2|weight"] = 20;

  // Exported overrides for: field_base.
  $overrides["field_base.field_related_people.settings|handler_submit"] = 'Change handler';

  // Exported overrides for: variable.
  $overrides["variable.context_status.value|publications_related_to_people"] = FALSE;

  // Exported overrides for: views_view.
  $overrides["views_view.uw_publications.display|block_1|display_options|defaults|filter_groups"] = FALSE;
  $overrides["views_view.uw_publications.display|block_1|display_options|defaults|filters"] = FALSE;
  $overrides["views_view.uw_publications.display|block_1|display_options|filters"] = array(
    'status' => array(
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'value' => 1,
      'group' => 1,
      'expose' => array(
        'operator' => FALSE,
      ),
    ),
    'type' => array(
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'value' => array(
        'biblio' => 'biblio',
      ),
      'group' => 1,
    ),
    'field_group_publication_value' => array(
      'id' => 'field_group_publication_value',
      'table' => 'field_data_field_group_publication',
      'field' => 'field_group_publication_value',
      'value' => array(
        'Yes' => 'Yes',
      ),
    ),
  );
  $overrides["views_view.uw_publications.display|block_2|display_options|defaults|filter_groups"] = FALSE;
  $overrides["views_view.uw_publications.display|block_2|display_options|defaults|filters"] = FALSE;
  $overrides["views_view.uw_publications.display|block_2|display_options|filters"] = array(
    'status' => array(
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'value' => 1,
      'group' => 1,
      'expose' => array(
        'operator' => FALSE,
      ),
    ),
    'type' => array(
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'value' => array(
        'biblio' => 'biblio',
      ),
      'group' => 1,
    ),
    'field_group_publication_value' => array(
      'id' => 'field_group_publication_value',
      'table' => 'field_data_field_group_publication',
      'field' => 'field_group_publication_value',
      'value' => array(
        'Yes' => 'Yes',
      ),
    ),
  );
  $overrides["views_view.uw_publications.display|block_2|display_options|title"] = 'Publications with this group';
  $overrides["views_view.uw_publications.display|default|display_options|empty"]["DELETED"] = TRUE;
  $overrides["views_view.uw_publications.display|person_profiles_publications_outside_group|display_options|defaults|filter_groups"] = FALSE;
  $overrides["views_view.uw_publications.display|person_profiles_publications_outside_group|display_options|defaults|filters"] = FALSE;
  $overrides["views_view.uw_publications.display|person_profiles_publications_outside_group|display_options|filters"] = array(
    'status' => array(
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'value' => 1,
      'group' => 1,
      'expose' => array(
        'operator' => FALSE,
      ),
    ),
    'type' => array(
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'value' => array(
        'biblio' => 'biblio',
      ),
      'group' => 1,
    ),
    'field_group_publication_value' => array(
      'id' => 'field_group_publication_value',
      'table' => 'field_data_field_group_publication',
      'field' => 'field_group_publication_value',
      'value' => array(
        'No' => 'No',
      ),
    ),
  );
  $overrides["views_view.uw_publications.display|project_publications|display_options|arguments"] = array(
    'field_related_projects_target_id' => array(
      'id' => 'field_related_projects_target_id',
      'table' => 'field_data_field_related_projects',
      'field' => 'field_related_projects_target_id',
      'default_action' => 'default',
      'default_argument_type' => 'node',
      'summary' => array(
        'format' => 'default_summary',
      ),
    ),
  );
  $overrides["views_view.uw_publications.display|project_publications|display_options|defaults|filter_groups"] = FALSE;
  $overrides["views_view.uw_publications.display|project_publications|display_options|defaults|filters"] = FALSE;
  $overrides["views_view.uw_publications.display|project_publications|display_options|filters"] = array(
    'status' => array(
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'value' => 1,
      'group' => 1,
      'expose' => array(
        'operator' => FALSE,
      ),
    ),
    'type' => array(
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'value' => array(
        'biblio' => 'biblio',
      ),
      'group' => 1,
    ),
    'field_group_publication_value' => array(
      'id' => 'field_group_publication_value',
      'table' => 'field_data_field_group_publication',
      'field' => 'field_group_publication_value',
      'value' => array(
        'Yes' => 'Yes',
      ),
    ),
  );
  $overrides["views_view.uw_publications.display|publications|display_options|defaults|filter_groups"] = FALSE;
  $overrides["views_view.uw_publications.display|publications|display_options|defaults|filters"] = FALSE;
  $overrides["views_view.uw_publications.display|publications|display_options|filters"] = array(
    'status' => array(
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'value' => 1,
      'group' => 1,
      'expose' => array(
        'operator' => FALSE,
      ),
    ),
    'type' => array(
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'value' => array(
        'biblio' => 'biblio',
      ),
      'group' => 1,
    ),
    'field_group_publication_value' => array(
      'id' => 'field_group_publication_value',
      'table' => 'field_data_field_group_publication',
      'field' => 'field_group_publication_value',
      'value' => array(
        'Yes' => 'Yes',
      ),
    ),
  );
  $overrides["views_view.uw_publications.display|publications|display_options|menu|title"] = 'Publications';
  $overrides["views_view.uw_publications.display|publications|display_options|menu|type"] = 'normal';

  return $overrides;
}
