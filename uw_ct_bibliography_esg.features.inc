<?php

/**
 * @file
 * uw_ct_bibliography_esg.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_bibliography_esg_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_context_default_contexts_alter().
 */
function uw_ct_bibliography_esg_context_default_contexts_alter(&$data) {
  if (isset($data['publications_related_to_people'])) {
    $data['publications_related_to_people']->reactions['block']['blocks']['views-7b25f6c74092e59ef43f1fdcd8aaedb9'] = array(
      'module' => 'views',
      'delta' => '7b25f6c74092e59ef43f1fdcd8aaedb9',
      'region' => 'content',
      'weight' => 21,
    ); /* WAS: '' */
    $data['publications_related_to_people']->reactions['block']['blocks']['views-uw_publications-block_2']['weight'] = 20; /* WAS: 47 */
  }
}

/**
 * Implements hook_field_default_field_bases_alter().
 */
function uw_ct_bibliography_esg_field_default_field_bases_alter(&$data) {
  if (isset($data['field_related_people'])) {
    $data['field_related_people']['settings']['handler_submit'] = 'Change handler'; /* WAS: '' */
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_ct_bibliography_esg_strongarm_alter(&$data) {
  if (isset($data['context_status'])) {
    $data['context_status']->value['publications_related_to_people'] = FALSE; /* WAS: TRUE */
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function uw_ct_bibliography_esg_views_default_views_alter(&$data) {
  if (isset($data['uw_publications'])) {
    $data['uw_publications']->display['block_1']->display_options['defaults']['filter_groups'] = FALSE; /* WAS: '' */
    $data['uw_publications']->display['block_1']->display_options['defaults']['filters'] = FALSE; /* WAS: '' */
    $data['uw_publications']->display['block_1']->display_options['filters'] = array(
      'status' => array(
        'id' => 'status',
        'table' => 'node',
        'field' => 'status',
        'value' => 1,
        'group' => 1,
        'expose' => array(
          'operator' => FALSE,
        ),
      ),
      'type' => array(
        'id' => 'type',
        'table' => 'node',
        'field' => 'type',
        'value' => array(
          'biblio' => 'biblio',
        ),
        'group' => 1,
      ),
      'field_group_publication_value' => array(
        'id' => 'field_group_publication_value',
        'table' => 'field_data_field_group_publication',
        'field' => 'field_group_publication_value',
        'value' => array(
          'Yes' => 'Yes',
        ),
      ),
    ); /* WAS: '' */
    $data['uw_publications']->display['block_2']->display_options['defaults']['filter_groups'] = FALSE; /* WAS: '' */
    $data['uw_publications']->display['block_2']->display_options['defaults']['filters'] = FALSE; /* WAS: '' */
    $data['uw_publications']->display['block_2']->display_options['filters'] = array(
      'status' => array(
        'id' => 'status',
        'table' => 'node',
        'field' => 'status',
        'value' => 1,
        'group' => 1,
        'expose' => array(
          'operator' => FALSE,
        ),
      ),
      'type' => array(
        'id' => 'type',
        'table' => 'node',
        'field' => 'type',
        'value' => array(
          'biblio' => 'biblio',
        ),
        'group' => 1,
      ),
      'field_group_publication_value' => array(
        'id' => 'field_group_publication_value',
        'table' => 'field_data_field_group_publication',
        'field' => 'field_group_publication_value',
        'value' => array(
          'Yes' => 'Yes',
        ),
      ),
    ); /* WAS: '' */
    $data['uw_publications']->display['block_2']->display_options['title'] = 'Publications with this group'; /* WAS: 'Publications' */
    $data['uw_publications']->display['person_profiles_publications_outside_group']->display_options['defaults']['filter_groups'] = FALSE; /* WAS: '' */
    $data['uw_publications']->display['person_profiles_publications_outside_group']->display_options['defaults']['filters'] = FALSE; /* WAS: '' */
    $data['uw_publications']->display['person_profiles_publications_outside_group']->display_options['filters'] = array(
      'status' => array(
        'id' => 'status',
        'table' => 'node',
        'field' => 'status',
        'value' => 1,
        'group' => 1,
        'expose' => array(
          'operator' => FALSE,
        ),
      ),
      'type' => array(
        'id' => 'type',
        'table' => 'node',
        'field' => 'type',
        'value' => array(
          'biblio' => 'biblio',
        ),
        'group' => 1,
      ),
      'field_group_publication_value' => array(
        'id' => 'field_group_publication_value',
        'table' => 'field_data_field_group_publication',
        'field' => 'field_group_publication_value',
        'value' => array(
          'No' => 'No',
        ),
      ),
    ); /* WAS: '' */
    $data['uw_publications']->display['project_publications']->display_options['arguments'] = array(
      'field_related_projects_target_id' => array(
        'id' => 'field_related_projects_target_id',
        'table' => 'field_data_field_related_projects',
        'field' => 'field_related_projects_target_id',
        'default_action' => 'default',
        'default_argument_type' => 'node',
        'summary' => array(
          'format' => 'default_summary',
        ),
      ),
    ); /* WAS: '' */
    $data['uw_publications']->display['project_publications']->display_options['defaults']['filter_groups'] = FALSE; /* WAS: '' */
    $data['uw_publications']->display['project_publications']->display_options['defaults']['filters'] = FALSE; /* WAS: '' */
    $data['uw_publications']->display['project_publications']->display_options['filters'] = array(
      'status' => array(
        'id' => 'status',
        'table' => 'node',
        'field' => 'status',
        'value' => 1,
        'group' => 1,
        'expose' => array(
          'operator' => FALSE,
        ),
      ),
      'type' => array(
        'id' => 'type',
        'table' => 'node',
        'field' => 'type',
        'value' => array(
          'biblio' => 'biblio',
        ),
        'group' => 1,
      ),
      'field_group_publication_value' => array(
        'id' => 'field_group_publication_value',
        'table' => 'field_data_field_group_publication',
        'field' => 'field_group_publication_value',
        'value' => array(
          'Yes' => 'Yes',
        ),
      ),
    ); /* WAS: '' */
    $data['uw_publications']->display['publications']->display_options['defaults']['filter_groups'] = FALSE; /* WAS: '' */
    $data['uw_publications']->display['publications']->display_options['defaults']['filters'] = FALSE; /* WAS: '' */
    $data['uw_publications']->display['publications']->display_options['filters'] = array(
      'status' => array(
        'id' => 'status',
        'table' => 'node',
        'field' => 'status',
        'value' => 1,
        'group' => 1,
        'expose' => array(
          'operator' => FALSE,
        ),
      ),
      'type' => array(
        'id' => 'type',
        'table' => 'node',
        'field' => 'type',
        'value' => array(
          'biblio' => 'biblio',
        ),
        'group' => 1,
      ),
      'field_group_publication_value' => array(
        'id' => 'field_group_publication_value',
        'table' => 'field_data_field_group_publication',
        'field' => 'field_group_publication_value',
        'value' => array(
          'Yes' => 'Yes',
        ),
      ),
    ); /* WAS: '' */
    $data['uw_publications']->display['publications']->display_options['menu']['title'] = 'Publications'; /* WAS: '' */
    $data['uw_publications']->display['publications']->display_options['menu']['type'] = 'normal'; /* WAS: '' */
    unset($data['uw_publications']->display['default']->display_options['empty']);
  }
}
